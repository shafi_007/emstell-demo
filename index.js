const express = require('express')
const bodyParser = require('body-parser');
const multer = require('multer');
const image = require ('imagemagick');

const uploadConfig = require('./upload')

const app = express()
const upload = multer(uploadConfig)

app.use(bodyParser.json());

app.post('/upload', upload.single('image'),(req,res)=>{

    image.convert([req.file.path, "-resize", "25x120", `name${Date.now}`], (err, stdout) => {
		if (err) {
			res.send(err);
		} else {
			res.json({message:'success'})
		}
	});

})

app.listen(3000, () => {
    console.log('App listening on port 3000!');
});